#include <iostream>
#include <Math.h>
using namespace std;

class Vector
{
private:
	double x;
	double y;
	double z;
public:
	Vector()
	{
		x = 0.0;
		y = 0.0;
		z = 0.0;
	}
	Vector(int _x, int _y, int _z) : x(_x), y(_y), z(_z)
	{}

	double gitLenght()
	{
		return sqrt(x * x + y * y + z * z);
	}
};

int main()
{
	Vector a(5, 15, 7);
	double len = a.gitLenght();
	cout << "The lenght of the vector = " << len << endl;
}